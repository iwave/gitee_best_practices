目录：<br/>
1.os git的作用<br/>
2.os git的使用<br/>
3.os git和jenkins的持续集成打包部署<br/>
<br/>

讲解：<br/>
1.os git的作用<br/>
    就是一个专门存储代码的云盘，加上一堆方便管理代码的各种功能组成的系统。<br/>
<br/>
2.os git的使用<br/>
    2.1两种方式，一种使用git客户端安装软件，一种是在ide开发工具中集成。<br/>
    2.2git客户端：https://gitee.com/all-about-git，此链接上有客户端下载，以及页面最下方有功能命令的介绍。<br/>
    2.3git和ide开发工具集成，请自行百度自己所用的开发工具名称+git配置，如：idea git配置、eclipse git配置。<br/>
    2.4功能使用：常用的两种功能，1更新，2提交，更新是指将云上的代码更新覆盖到本地来，提交是指将自己新写的代码覆盖到云上去。<br/>
    2.5分支、合并等功能请详细查看os git功能使用https://gitee.com/help#article-header0，没有什么文档比官方的更标准了，其他小众需求和功能请自行百度关键字。<br/>
<br/>
3.os git和jenkins的持续集成打包部署<br/>
    3.1jenkins的作用：在服务器上自动拉取最新代码进行打包部署。<br/>
    3.2os git和jenkins的集成：<br/>
    ![输入图片说明](https://images.gitee.com/uploads/images/2019/0617/102743_70c8862f_349977.jpeg "3344.jpg")<br/>
<br/>
以上就是os git作用、使用、集成打包的讲解。<br/>